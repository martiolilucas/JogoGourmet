﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JogoGourmet_Csharp.Tests
{
    [TestClass()]
    public class DishesTreeParserTests
    {
        [TestMethod()]
        public void DishesTreeParserTest_Instance()
        {

            string root_name = "1";
            var root = new DishesTreeNode(root_name);
            var dishesTree = new DishesTree(root);

            var dishesTreeParser = new DishesTreeParser(dishesTree);

            Assert.AreEqual(root, dishesTreeParser.CurrentNode);
        }

        [TestMethod()]
        public void GetCurrentParentTest()
        {
            var leftNode = new DishesTreeNode("2");
            var root = new DishesTreeNode("1", leftNode, null);
            var dishesTree = new DishesTree(root);
            var dishesTreeParser = new DishesTreeParser(dishesTree);

            var parentNode = dishesTreeParser.GetCurrentParent();

            Assert.AreEqual(null, parentNode);

            dishesTreeParser.NextLeft();
            parentNode = dishesTreeParser.GetCurrentParent();

            Assert.AreEqual(root, parentNode);
        }

        [TestMethod()]
        public void HasNextRightTest()
        {
            var leftNode = new DishesTreeNode("2");
            var root = new DishesTreeNode("1", leftNode, null);
            var dishesTree = new DishesTree(root);
            var dishesTreeParser = new DishesTreeParser(dishesTree);

            Assert.IsFalse(dishesTreeParser.HasNextRight());

            var rightNode = new DishesTreeNode("3");
            root.Right = rightNode;

            Assert.IsTrue(dishesTreeParser.HasNextRight());
        }

        [TestMethod()]
        public void HasNextLeftTest()
        {
            var rightNode = new DishesTreeNode("2");
            var root = new DishesTreeNode("1", null, rightNode);
            var dishesTree = new DishesTree(root);
            var dishesTreeParser = new DishesTreeParser(dishesTree);

            Assert.IsFalse(dishesTreeParser.HasNextLeft());

            var leftNode = new DishesTreeNode("3");
            root.Left = leftNode;

            Assert.IsTrue(dishesTreeParser.HasNextLeft());
        }

        [TestMethod()]
        public void CurrentIsALeafTest()
        {
            var rightNode = new DishesTreeNode("2");
            var leftNode = new DishesTreeNode("3");
            var root = new DishesTreeNode("1", leftNode, rightNode);
            var dishesTree = new DishesTree(root);
            var dishesTreeParser = new DishesTreeParser(dishesTree);

            Assert.IsFalse(dishesTreeParser.CurrentIsALeaf());

            dishesTreeParser.NextLeft();

            Assert.IsTrue(dishesTreeParser.CurrentIsALeaf());

            dishesTreeParser.Back();
            dishesTreeParser.NextRight();

            Assert.IsTrue(dishesTreeParser.CurrentIsALeaf());
        }

        [TestMethod()]
        public void NextLeftTest()
        {
            var llNode = new DishesTreeNode("2.1");
            var leftNode = new DishesTreeNode("2", llNode, null);
            var root = new DishesTreeNode("1", leftNode, null);
            var dishesTree = new DishesTree(root);
            var dishesTreeParser = new DishesTreeParser(dishesTree);

            dishesTreeParser.NextLeft();
            dishesTreeParser.NextLeft();

            Assert.AreEqual(llNode, dishesTreeParser.CurrentNode);
        }

        [TestMethod()]
        public void NextRightTest()
        {
            var rrNode = new DishesTreeNode("2.1");
            var rightNode = new DishesTreeNode("2", null, rrNode);
            var root = new DishesTreeNode("1", null, rightNode);
            var dishesTree = new DishesTree(root);
            var dishesTreeParser = new DishesTreeParser(dishesTree);

            dishesTreeParser.NextRight();
            dishesTreeParser.NextRight();

            Assert.AreEqual(rrNode, dishesTreeParser.CurrentNode);
        }

        [TestMethod()]
        public void BackTest()
        {
            var rightNode = new DishesTreeNode("2");
            var root = new DishesTreeNode("1", null, rightNode);
            var dishesTree = new DishesTree(root);
            var dishesTreeParser = new DishesTreeParser(dishesTree);

            //dishesTreeParser.NextRight();
            dishesTreeParser.Back();

            Assert.AreEqual(root, dishesTreeParser.CurrentNode);
        }
    }
}