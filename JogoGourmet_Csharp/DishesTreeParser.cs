﻿using System.Collections.Generic;

namespace JogoGourmet_Csharp
{
    public class DishesTreeParser
    {
        private DishesTree _dishesTree;
        private readonly System.Collections.Generic.Stack<DishesTreeNode> _parseStack;

        public DishesTreeNode CurrentNode { get; set; }

        public DishesTreeParser(DishesTree dishesTree)
        {
            _dishesTree = dishesTree;
            CurrentNode = DishesTree.Root;
            _parseStack = new Stack<DishesTreeNode>();
        }

        public DishesTreeNode GetCurrentParent()
        {
            DishesTreeNode currentParent = null;
            if (_parseStack.Count <= 0) return null;
            currentParent = _parseStack.Pop();
            _parseStack.Push(currentParent);
            return currentParent;
        }

        public bool HasNextRight()
        {
            return CurrentNode.Right != null;
        }

        public bool HasNextLeft()
        {
            return CurrentNode.Left != null;
        }

        public bool CurrentIsALeaf()
        {
            return !HasNextLeft() && !HasNextRight();
        }
        public DishesTreeNode NextLeft()
        {
            _parseStack.Push(CurrentNode);
            var next = CurrentNode.Left;
            CurrentNode = next;

            return next;
        }

        public DishesTreeNode NextRight()
        {
            _parseStack.Push(CurrentNode);
            var next = CurrentNode.Right;
            CurrentNode = next;
            return next;
        }

        public DishesTreeNode Back()
        {
            if (_parseStack.Count > 0)
                CurrentNode = _parseStack.Pop();
            return CurrentNode;
        }


    }
}
