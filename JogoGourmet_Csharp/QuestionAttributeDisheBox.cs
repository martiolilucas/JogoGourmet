﻿using System;
using System.Windows.Forms;

namespace JogoGourmet_Csharp
{
    public partial class QuestionAttributeDisheBox : Form
    {
        public QuestionAttributeDisheBox(string question)
        {
            InitializeComponent();
            question_label.Text = question;

        }

        public string InputQuality { get; set; }

        private void OK_button_Click(object sender, EventArgs e)
        {
            InputQuality = question_answer.Text.Trim();
            Close();
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
