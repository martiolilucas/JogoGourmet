﻿namespace JogoGourmet_Csharp
{
    public class DishesTree
    {
        public static DishesTreeNode Root { get; private set; }

        public DishesTree(DishesTreeNode node)
        {
            Root = node;
        }

        public static void InsertNewDishe(DishesTreeNode parentNode, string newDisheName, string qualityDishe)
        {
            var currentNode = parentNode.Right;
            var newDishe = new DishesTreeNode(newDisheName);
            var newQuality = new DishesTreeNode(qualityDishe, newDishe, currentNode);
            parentNode.Right = newQuality;

        }
    }
}
