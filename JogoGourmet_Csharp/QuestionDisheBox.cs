﻿using System;
using System.Windows.Forms;

namespace JogoGourmet_Csharp
{
    public partial class QuestionDisheBox : Form
    {
        public QuestionDisheBox()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        public string InputName { get; set; }

        private void OK_button_Click(object sender, EventArgs e)
        {

            InputName = name_input.Text.Trim();
            Close();
        }

        private void cancel_button_Click(object sender, EventArgs e)
        {
            Close();

        }
    }
}
