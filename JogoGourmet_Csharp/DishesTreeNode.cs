﻿using System.Collections.Generic;

namespace JogoGourmet_Csharp
{
    public class DishesTreeNode
    {
        public DishesTreeNode(string name, DishesTreeNode left, DishesTreeNode right)
        {
            Name = name;
            Left = left;
            Right = right;
        }
        public DishesTreeNode(string name)
        {
            Name = name;
            Left = null;
            Right = null;
        }

        public string Name { get; set; }

        public DishesTreeNode Left { get; set; }
        public DishesTreeNode Right { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            return obj is DishesTreeNode node &&
                   Name == node.Name &&
                   EqualityComparer<DishesTreeNode>.Default.Equals(Left, node.Left) &&
                   EqualityComparer<DishesTreeNode>.Default.Equals(Right, node.Right);
        }
    }
}
