﻿using System.Windows;

namespace JogoGourmet_Csharp
{
    public class JogoGourmet
    {
        private bool _gotIt;
        private bool _giveUp;
        private readonly DishesTree _tree;
        private DishesTreeParser _dishesTreeParser;

        public JogoGourmet()
        {
            var left = new DishesTreeNode("Lasanha");
            var right = new DishesTreeNode("Bolo de Chocolate");
            var root = new DishesTreeNode("massa", left, right);

            _tree = new DishesTree(root);
        }

        public void Play()
        {

            while (MessageBox.Show("Pense em um prato que gosta", "Jogo Gourmet", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                StartNewRound();
                while (!_gotIt && !_giveUp)
                {
                    var isThisOne = MessageBox.Show("O prato que você pensou é " + _dishesTreeParser.CurrentNode.Name + "?", "Jogo Gourmet", MessageBoxButton.YesNo) == MessageBoxResult.Yes;
                    if (WeHaveAVeredict())
                    {
                        if (isThisOne)
                        {
                            GotYou();
                            MessageBox.Show("Acertei de novo!");

                        }
                        else
                        {
                            YouGotMe();
                            TellMeWhichOneIs();

                        }
                    }
                    else
                    {
                        GoingToTheNext(isThisOne);
                    }
                }
            }
        }

        private bool WeHaveAVeredict()
        {
            return _dishesTreeParser.CurrentIsALeaf();
        }

        private void GotYou()
        {
            _gotIt = true;
        }

        private void YouGotMe()
        {
            _giveUp = true;
        }

        private void StartNewRound()
        {
            _dishesTreeParser = new DishesTreeParser(_tree);
            _gotIt = false;
            _giveUp = false;
        }

        private void TellMeWhichOneIs()
        {
            var questionDisheBox = new QuestionDisheBox();
            questionDisheBox.ShowDialog();

            var newDisheName = questionDisheBox.InputName;

            var questionAttributeDisheBox = new QuestionAttributeDisheBox(newDisheName + " é ______ mas " + _dishesTreeParser.CurrentNode.Name + " não. ");
            questionAttributeDisheBox.ShowDialog();

            DishesTree.InsertNewDishe(_dishesTreeParser.GetCurrentParent(), newDisheName, questionAttributeDisheBox.InputQuality);

        }

        private void GoingToTheNext(bool isThisOne)
        {
            if (isThisOne && _dishesTreeParser.HasNextLeft())
            {
                _dishesTreeParser.NextLeft();
            }
            else if (_dishesTreeParser.HasNextRight())
            {
                _dishesTreeParser.NextRight();
            }
            else
            {
                _dishesTreeParser.Back();
                _dishesTreeParser.NextRight();
            }
        }
    }
}
