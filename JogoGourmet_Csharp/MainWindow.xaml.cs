﻿using System.Windows;

namespace JogoGourmet_Csharp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Hide();
            var game = new JogoGourmet();
            game.Play();
            this.Close();
        }
    }
}
